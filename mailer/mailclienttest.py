import smtplib
from email import encoders
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart

server = smtplib.SMTP('smtp.gmail.com', 587)

server.ehlo()
server.starttls()

with open('password.txt', 'r') as f:
	password = f.read()

server.login('111elevenvan@gmail.com', password)

msg = MIMEMultipart()
msg['From'] = 'elevenvan'
msg['To'] = '111elevenvan@gmail.com'
msg['Subject'] = 'test email'

#plain text message

with open('message.txt', 'r') as f:
	message = f.read()

msg.attach(MIMEText(message,'plain'))


#Image attachment
filename = 'wizard.pdf'
attachment = open(filename, 'rb')

p = MIMEBase('application', 'octet-stream')
p.set_payload(attachment.read())

encoders.encode_base64(p)
p.add_header('Content-Disposition', 'attachment', filename=filename)
msg.attach(p)

text = msg.as_string()
server.sendmail('111elevenvan@gmail.com', 'tejuco.elverisa@gmail.com', text)