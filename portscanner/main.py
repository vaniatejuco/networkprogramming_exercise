import socket
import threading
from queue import Queue


target = "192.168.1.1"
queue = Queue()
open_ports = []


def portscan(port):
	try:
	   	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	   	sock.connect((target,port))
	   	return True

	except:
		return False

#print(portscan(98))

# for port in range(1, 1024):
# 	result = portscan(port)
# 	if result:
# 		print('Port {} is open!'.format(port))
# 	else:
# 		print("Port {} is closed!".format(port))

def fill_queue(port_list):
	for port in port_list:
		queue.put(port)

#the process to thread
def worker():
	while not queue.empty():
		port = queue.get()
		if portscan(port):
			print("Port {} is open!".format(port))
			open_ports.append(port)


#use queue so there won't be double scan
#filling queau
port_list = range(1, 1024)
fill_queue(port_list)


#making threads

thread_list = []

for t in range(50):
	thread = threading.Thread(target=worker)
	thread_list.append(thread)


#starting the threads
for thread in thread_list:
	thread.start()

#this is so that threads will wait that all of them are finished before moving to the next process
for thread in thread_list:
	thread.join()

print("Open ports are: ", open_ports)
